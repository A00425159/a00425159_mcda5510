/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csvmerger;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.LinkedList;
import java.util.Iterator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVPrinter;
import java.util.logging.StreamHandler;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author danielarantes
 */
public class CSVMerger {
    private final LinkedList<CSVRecord> recList;
    private int invalidRecNum = 0;
    private int validRecNum = 0;
    private Logger logger;
    //CSV file header
    private static final Object [] FILE_HEADER = {"First Name","Last Name","Street Number" ,"Street", "City", "Province", "Postal Code", "Country" , "Phone Number", "email Address"};
    
    // Loads the valid records from a file into the list
    public void loadFile(File file) {
        try(Reader in = new FileReader(file)) {
            Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
            Iterator<CSVRecord> iter = records.iterator();
            iter.next(); // skips the header
            while(iter.hasNext()){
                CSVRecord record = iter.next();
                if(isValidRecord(file, record, 10)){
                    recList.add(record);
                }
            }			
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean isValidRecord(File file, CSVRecord record, int rowRecs) {

        if(record.size() != rowRecs){
            invalidRecNum++;
            logger.log(Level.WARNING, "INVALID NUMBER OF RECORDS: {1} instead of {2} - File: {0}", new Object[]{file, record.size(), rowRecs});
            return false;
        }
        else {
            Iterator<String> iter = record.iterator();
            int rownum = 1;
            String col = "";
            while(iter.hasNext()){
                col = iter.next();
                if(col.length() == 0){
                    invalidRecNum++;
                    logger.log(Level.WARNING, "INVALID COLUMN LENGTH ON COLUMN {1}: {2} - File: {0}", new Object[]{file, rownum, col.length()});
                    return false;
                }
                rownum++;
            }
        }
        validRecNum++;
        return true;
    }

    public void walk(String path, File output) {

        File root = new File(path);
        File[] list = root.listFiles();

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk(f.getAbsolutePath(), output);
            }
            else {
//                logger.log(Level.INFO, "Size of list of records: {0}", this.recList.size());                
                loadFile(f);
            }
        }
    }
    
    public void writeOutput(File file){
        try(CSVPrinter printer = new CSVPrinter(new FileWriter(file), CSVFormat.DEFAULT)){
            //Create CSV file header
            printer.printRecord(FILE_HEADER);
            //Iterator<CSVRecord> iter = recList.iterator();
            for(CSVRecord record : recList){
                printer.printRecord(record);
            }
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final long startTime = System.currentTimeMillis();
        File output = new File("/home/danielarantes/0_mcda/5510/NewFolder/CSVMerger/output.csv");
        CSVMerger merger = new CSVMerger();
        merger.walk("/home/danielarantes/0_mcda/5510/NewFolder/CSVMerger/sampledata", output);
        merger.writeOutput(output);
        final long endTime = System.currentTimeMillis();
        try{
            Thread.sleep(1000);
        }
        catch(InterruptedException ie){
            ie.printStackTrace();
        }
        merger.logger.log(Level.INFO, "\n\nFinal - Size of list records size: {0}", merger.recList.size());
        merger.logger.log(Level.INFO, "Number of invalid records: {0}", merger.invalidRecNum);
        merger.logger.log(Level.INFO, "Number of valid records: {0}", merger.validRecNum);
        System.out.println("\n\n\t\tTotal execution time: " + (endTime - startTime) +" ms");
    }

    public CSVMerger() {
        this.recList = new LinkedList();
        this.logger = Logger.getAnonymousLogger();
        this.logger.setLevel(Level.ALL);
        logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()));
    }
    
}


    
//    public static HashMap walk2(HashMap map, String path) {
//
//        File root = new File( path );
//        File[] list = root.listFiles();
//
//        for ( File f : list ) {
//            if ( f.isDirectory() ) {
////                System.out.println( "Dir:" + f.getAbsoluteFile() );
//                map.put(f, null);
////                System.out.println(map);
//                map = walk2(map, f.getAbsolutePath());
//            }
//            else {
////                System.out.println(map);
////                System.out.println( "File:" + f.getAbsoluteFile() );
//                LinkedList filelist = (LinkedList)map.get(f.getParentFile());
//                if(filelist == null){
//                    filelist = new LinkedList();
//                    map.put(f.getParentFile(), filelist);
//                }
////                System.out.println(f.getParentFile().getName() + "  -  " + f.getName()); 
//
//                filelist.add(f);
//            }
//        }
//        return map;
//        
//    }

//    public static void printMap(HashMap map){
//        //        System.out.println(map);
//        Iterator i = map.keySet().iterator();
//        Iterator j = null;
//        LinkedList filelist = null;
//        int fileCounter = 0;
//        while(i.hasNext()){
//            File dir = (File)i.next();
//            System.out.println("Dir: " + dir);
//            filelist = (LinkedList)map.get(dir);
//            if(filelist != null){
//                j = filelist.iterator();
//                if(j != null){
//                    while(j.hasNext()){
//                        File file = (File)j.next();
//                        System.out.println("\t\tFile: " + file);
//                        fileCounter++;
//                    }
//                }
//            } else {
//                System.out.println("\n\n==========================================");
//                System.out.println("\tdir: " + dir + " filelist = " + filelist);
//                System.out.println("==========================================\n\n");
//            }
//        }
//        System.out.println("\n\n\n\t\tNumber of files: " + fileCounter);
//    }

//    public static void walk( String path ) {
//
//        File root = new File( path );
//        File[] list = root.listFiles();
//        HashMap map = new HashMap();
//
//        if (list == null) return;
//
//        for ( File f : list ) {
//            if ( f.isDirectory() ) {
////                System.out.println( "Dir:" + f.getAbsoluteFile() );
//                map.put(f, null);
////                System.out.println(map);
//                walk( f.getAbsolutePath() );
//            }
//            else {
////                System.out.println(map);
////                System.out.println( "File:" + f.getAbsoluteFile() );
//                LinkedList filelist = (LinkedList)map.get(f.getParentFile());
//                if(filelist == null){
//                    filelist = new LinkedList();
//                    map.put(f.getParentFile(), filelist);
//                }
////                System.out.println(filelist);    
//                filelist.add(f);
//            }
//        }
////        System.out.println(map);
//        Iterator i = map.keySet().iterator();
//        Iterator j = null;
//        LinkedList filelist = null;
//        int fileCounter = 0;
//        while(i.hasNext()){
//            File dir = (File)i.next();
//            System.out.println("Dir: " + dir);
//            filelist = (LinkedList)map.get(dir);
//            j = filelist.iterator();
//            while(j.hasNext()){
//                File file = (File)j.next();
//                System.out.println("\t\tFile: " + file);
//                fileCounter++;
//            }
//        }
//        System.out.println("\n\n\n\t\tNumber of files: " + fileCounter);
//    }
//    


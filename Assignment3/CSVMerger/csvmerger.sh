#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Please inform the top level dir to start the csv merge."
    exit 1
fi

topdir=$1
outfile="outfile.csv"
touch $outfile

for f in `find $topdir -type f`
do
    cat $f | sed 1d >> $outfile
done

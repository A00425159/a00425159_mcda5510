﻿//  File ContactGenerator
//  Sample code was taken from:
//  http://www.csharpprogramming.tips/2013/06/RandomDoxGenerator.html
//  Other useful methods are there.
//
// Requirements:
// Exapand on the below example to create a CSV file (https://en.wikipedia.org/wiki/Comma-separated_values)
// For contacts with the following data
// First Name
// Last Name
// Street Number
// City
// Province
// Country  == Canada ( Simply insert "canada")
// Postal Code  ( they can be read form a file for this example if you choose, or generate if you wish)
// Phone Number ( they can be read form a file for this example if you choose, or generate if you wish)
// email Address ( Append firstname.lastname against a series for domain names read for a file
//
// Please always try to write clean and readable code
// Here is a good reference doc http://ricardogeek.com/docs/clean_code.html  
// Submit to Bitbucket under Assignment1

// 

using System;
using System.IO;

// Describes what is a namespace 
// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/namespaces/

namespace SimpleConsoleApp
{
    class ContactGenerator
    {
        // instance of random number generator
        Random rand = new Random();

        static void Main(string[] args)
        {
            // instance of ContactGenerator
            ContactGenerator dg = new ContactGenerator();
        }

        public ContactGenerator()
        {
            String COMMA = ",";
            String outputFileName = @".\customers.csv";

            if (File.Exists(outputFileName))
            {
                Console.Write(" File " + outputFileName + " exists, appending");
            }
            StreamWriter fileStream = new StreamWriter(outputFileName, true);

            // Write Header
            fileStream.Write("First Name");
            fileStream.Write(COMMA);
            fileStream.Write("Last Name");
            fileStream.Write(COMMA);
            fileStream.Write("Street Number");
            fileStream.Write(COMMA);
            fileStream.Write("City");
            fileStream.Write(COMMA);
            fileStream.Write("Province");
            fileStream.Write(COMMA);
            fileStream.Write("Country");
            fileStream.Write(COMMA);
            fileStream.Write("Postal Code");
            fileStream.Write(COMMA);
            fileStream.Write("Phone Number");
            fileStream.Write(COMMA);
            fileStream.Write("Email Address");

            fileStream.WriteLine();
            String firstName = "";
            String lastName = "";
            String postalCode = "";
            for (int i = 0; i < 20; i++)
            {
                firstName = GenerateFirstName();
                lastName = GenerateLastName();
                postalCode = GeneratePostalCode();

                fileStream.Write(firstName);
                fileStream.Write(COMMA);
                fileStream.Write(lastName);
                fileStream.Write(COMMA);
                fileStream.Write(GetAddress(postalCode));
                fileStream.Write(COMMA);
                fileStream.Write(GenerateCity());
                fileStream.Write(COMMA);
                fileStream.Write(GenerateProvince());
                fileStream.Write(COMMA);
                fileStream.Write(GenerateCountry());
                fileStream.Write(COMMA);
                fileStream.Write(postalCode);
                fileStream.Write(COMMA);
                fileStream.Write(GeneratePhoneNumber());
                fileStream.Write(COMMA);
                fileStream.Write(GenerateEmail(firstName, lastName));
                fileStream.WriteLine();
            }
            fileStream.Close();
        }

        // Gets a random first name from a file.
        public string GenerateFirstName()
        {
            String firstNames = @".\firstNames.txt";
            
            return ReturnRandomLine(firstNames);
        }

        // Gets a random last name from a file.
        public string GenerateLastName()
        {
            String lastNames = @".\lastNames.txt";
            return ReturnRandomLine(lastNames);
        }
			
        // Generates the email given the first and last name. Reads domains from a file.
		public string GenerateEmail(String firstName, String lastName)
		{
			String domains = @".\emailDomains.txt";
            return (firstName + "." + lastName + "@" + ReturnRandomLine(domains)).Replace(" ", "");
		}

        // return Halifax
		public string GenerateCity(){
			return "Halifax";
		}

        // return Nova Scotia
		public string GenerateProvince(){
			return "NS";
		}

        // return Canada
		public string GenerateCountry(){
			return "Canada";
		}

        // Get a VALID Halifax postal code from a file.
        // The file was created using R from the site: http://www.findthepostalcode.com/location.php?province=NS&location=HALIFAX
        public string GeneratePostalCode()
        {
            String postalcodes = @".\postalcodes.txt";
            return ReturnRandomLine(postalcodes);
        }

        // Given a postal code, search for a VALID address in the postal code file
        // The files were created in R from the site: http://www.findthepostalcode.com/code.php?c=POSTAL_CODE
        public string GetAddress(String postalCode)
        {
            String postalCodeAddresses = @".\addresses\" + postalCode.Replace(" ","");
            return ReturnRandomLine(postalCodeAddresses);
        }

        // Generates a random phone number (possibly invalid).
        public string GeneratePhoneNumber()
        {
            return "902 " + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + " " + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9) + rand.Next(0, 9);
        }

        // Gets a random line from a file
        // The original code used to position to a random byte in the file, then return the next line from it.
        // This new function reads all lines (which is not a good idea for large-ish files) and then chose one random line
        public string ReturnRandomLine(string FileName)
        {
            string sReturn = string.Empty;

            // don't do that for large files...not a good idea
            // I changed the approach to read random lines within the number of lines
            // because if the address file had just 1 line the code would fail miserably (keeps calling the random line function until it gets a nasty error)
            // ...and there are quite a few postal codes with just 1 address in Halifax
            String[] lines = File.ReadAllLines(FileName);
            sReturn = lines[rand.Next(1, lines.Length) - 1];
            return sReturn;
        }
    }
}
USE [master]
GO
/****** Object:  Database [testdb]    Script Date: 2/10/2018 7:11:52 PM ******/
CREATE DATABASE [testdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'testdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\testdb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'testdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\testdb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [testdb] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [testdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [testdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [testdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [testdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [testdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [testdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [testdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [testdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [testdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [testdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [testdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [testdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [testdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [testdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [testdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [testdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [testdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [testdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [testdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [testdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [testdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [testdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [testdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [testdb] SET RECOVERY FULL 
GO
ALTER DATABASE [testdb] SET  MULTI_USER 
GO
ALTER DATABASE [testdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [testdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [testdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [testdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [testdb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'testdb', N'ON'
GO
ALTER DATABASE [testdb] SET QUERY_STORE = OFF
GO
USE [testdb]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [testdb]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 2/10/2018 7:11:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](64) NULL,
	[LastName] [varchar](64) NULL,
	[StreetNumber] [int] NULL,
	[Address] [varchar](64) NULL,
	[City] [varchar](64) NULL,
	[Province] [varchar](64) NULL,
	[Country] [varchar](64) NULL,
	[PostalCode] [varchar](10) NULL,
	[PhoneNumber] [varchar](64) NULL,
	[EmailAddress] [varchar](64) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (2, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (3, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (4, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (5, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (6, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (7, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (8, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (9, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (10, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (11, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (12, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (13, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (14, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (15, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (16, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (17, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (18, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (19, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (20, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (21, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (22, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (23, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (24, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (25, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (26, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (27, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (28, N'Hanieh', N'Arantes12345678901234567890123456789012345678901234567', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7123', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (29, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (30, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (31, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (32, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (33, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (34, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (35, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (36, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (37, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (38, N'Abhiraj', N'Singh', 58, N'Ardwell Ave blablablabla blablablabla 123 blablablabla', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (39, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0122', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (40, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (41, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent Road Avenua Crescent big string', N'Halifax', N'NS', N'Canada', N'B3M3B122', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (42, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (43, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (44, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (45, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (46, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (47, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (48, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (49, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (50, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (51, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (52, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (53, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (54, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (55, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (56, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (57, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (58, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (59, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (60, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (61, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (62, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (63, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (64, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (65, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (66, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (67, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (68, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (69, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (70, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (71, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (72, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (73, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (74, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (75, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (76, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (77, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (78, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (79, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (80, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (81, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (82, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (83, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (84, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (85, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (86, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (87, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (88, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (89, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (90, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (91, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (92, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (93, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (94, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (95, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (96, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (97, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (98, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (99, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (100, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
GO
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (101, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (102, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (103, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (104, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (105, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (106, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (107, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (108, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (109, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (110, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (111, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (112, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (113, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (114, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (115, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (116, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (117, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (118, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (119, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (120, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (121, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (122, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (123, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (124, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (125, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (126, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (127, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (128, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (129, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (130, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (131, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (132, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (133, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (134, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (135, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (136, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (137, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (138, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (139, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (140, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (141, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (142, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (143, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (144, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (145, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (146, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (147, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (148, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (149, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (150, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (151, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (152, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (153, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (154, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (155, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (156, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (157, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (158, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (159, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (160, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (161, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (162, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (163, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (164, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (165, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (166, N'Rodolfo', N'Mishra', 1666, N'Bedford Row', N'Halifax', N'NS', N'Canada', N'B3J2P8', N'902 640 5718', N'Rodolfo.Mishra@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (167, N'Sankalp', N'Garcia Barrantes', 290, N'Main Ave', N'Halifax', N'NS', N'Canada', N'B3M3V3', N'902 744 4678', N'Sankalp.GarciaBarrantes@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (168, N'Amrit', N'Abdrashitov', 21, N'Parkland Dr', N'Halifax', N'NS', N'Canada', N'B3S1S9', N'902 674 3007', N'Amrit.Abdrashitov@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (169, N'Yudhvir', N'Oliver', 7, N'Flint St', N'Halifax', N'NS', N'Canada', N'B3N2V3', N'902 045 3417', N'Yudhvir.Oliver@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (170, N'Amrit', N'Mishra', 19, N'Plateau Crescent', N'Halifax', N'NS', N'Canada', N'B3M3K9', N'902 167 3436', N'Amrit.Mishra@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (171, N'Vivek', N'Kumar', 45, N'Aster Ct', N'Halifax', N'NS', N'Canada', N'B3S1G5', N'902 641 0310', N'Vivek.Kumar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (172, N'Hanieh', N'Arantes', 6140, N'Allan St', N'Halifax', N'NS', N'Canada', N'B3L1G7', N'902 153 5177', N'Hanieh.Arantes@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (173, N'Rodolfo', N'Salvi', 5660, N'Kane St', N'Halifax', N'NS', N'Canada', N'B3K2B5', N'902 248 4707', N'Rodolfo.Salvi@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (174, N'Daune', N'Salvi', 188, N'Dunbrack St', N'Halifax', N'NS', N'Canada', N'B3M3L8', N'902 876 5430', N'Daune.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (175, N'Dinesh Kumar', N'Omar', 5433, N'Cornwallis St', N'Halifax', N'NS', N'Canada', N'B3K1A8', N'902 014 6566', N'DineshKumar.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (176, N'Naveenraj', N'Singh', 13, N'Keystone Ct', N'Halifax', N'NS', N'Canada', N'B3N3B4', N'902 253 5175', N'Naveenraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (177, N'Lourdes', N'Hamza', 200, N'Willett St', N'Halifax', N'NS', N'Canada', N'B3M3C5', N'902 016 7620', N'Lourdes.Hamza@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (178, N'Naveenraj', N'Vechoor Padmanabhan', 6848, N'Chebucto Rd', N'Halifax', N'NS', N'Canada', N'B3L1M3', N'902 508 8057', N'Naveenraj.VechoorPadmanabhan@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (179, N'Naghmeh', N'Singh', 5692, N'Bilby St', N'Halifax', N'NS', N'Canada', N'B3K1V5', N'902 037 4878', N'Naghmeh.Singh@yahoo.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (180, N'Md. Wahid Tausif', N'Garcia Barrantes', 69, N'Castlepark Grove', N'Halifax', N'NS', N'Canada', N'B3M4X8', N'902 873 8716', N'Md.WahidTausif.GarciaBarrantes@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (181, N'Sankalp', N'Omar', 5714, N'Southwood Dr', N'Halifax', N'NS', N'Canada', N'B3H1E6', N'902 763 3541', N'Sankalp.Omar@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (182, N'Abhiraj', N'Singh', 58, N'Ardwell Ave', N'Halifax', N'NS', N'Canada', N'B3R1L7', N'902 356 3062', N'Abhiraj.Singh@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (183, N'Caner Adil', N'Nagamanickam', 102, N'Hartlin Settlement Rd', N'Halifax', N'NS', N'Canada', N'B0J1W0', N'902 273 8187', N'CanerAdil.Nagamanickam@hotmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (184, N'Narendran', N'Salvi', 43, N'Chipstone Close', N'Halifax', N'NS', N'Canada', N'B3M4L4', N'902 341 0045', N'Narendran.Salvi@gmail.com')
INSERT [dbo].[Customer] ([id], [FirstName], [LastName], [StreetNumber], [Address], [City], [Province], [Country], [PostalCode], [PhoneNumber], [EmailAddress]) VALUES (185, N'Ildar', N'Bhagat', 3, N'Kingfisher Crescent', N'Halifax', N'NS', N'Canada', N'B3M3B1', N'902 432 3356', N'Ildar.Bhagat@gmail.com')
SET IDENTITY_INSERT [dbo].[Customer] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Customer table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Customer'
GO
USE [master]
GO
ALTER DATABASE [testdb] SET  READ_WRITE 
GO

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Project -> Add references
// To use the csv parser
using Microsoft.VisualBasic.FileIO;

namespace WindowsContactForm
{
    public partial class ContactForm : Form
    {
        int index = 0;
        Dictionary<int, Customer> allData = new Dictionary<int, Customer>();
        //int RowIdxFirstName = 0;
        int currentIndex = 0;
        int maxIndex = 0;

        public ContactForm()
        {
            InitializeComponent();
        }

        private void prevButtonClick(object sender, EventArgs e)
        {
            
            index = ((currentIndex - 1) % allData.Count);
            if(index < 0)
            {
                index = allData.Count - 1;
            }
            currentIndex = index;
            UpdateData(currentIndex);
        }

        private void nextButtonClick(object sender, EventArgs e)
        {
            index = (currentIndex + 1) % allData.Count;
            currentIndex = index;
            UpdateData(currentIndex);
        }

        private void ContactForm_Load(object sender, EventArgs e)
        {
            allData.Clear();
            index = 0;
            using (CustomerDataModel db = new CustomerDataModel())
            {
                DbSet<Customer> customers = db.Customers;

                foreach (Customer customer in customers)
                {
                    allData.Add(index, customer);
                    UpdateData(index);
                    index++;
                }
            }
            currentIndex = index - 1;
            maxIndex = currentIndex;
        }

        private void resetFields()
        {
            richTextBoxValidation.Text = "";
            this.textBoxID.BackColor = System.Drawing.Color.LightGray;
            this.textBoxFirstName.BackColor = System.Drawing.Color.White;
            this.textBoxLastName.BackColor = System.Drawing.Color.White;
            this.textBoxPhoneNumber.BackColor = System.Drawing.Color.White;
            this.textBoxStreetNumber.BackColor = System.Drawing.Color.White;
            this.textBoxAddress.BackColor = System.Drawing.Color.White;
            this.textBoxCity.BackColor = System.Drawing.Color.White;
            this.textBoxProvince.BackColor = System.Drawing.Color.White;
            this.textBoxCountry.BackColor = System.Drawing.Color.White;
            this.textBoxEmailAddress.BackColor = System.Drawing.Color.White;
            this.textBoxPostalCode.BackColor = System.Drawing.Color.White;
            this.textBoxFileName.BackColor = System.Drawing.Color.White;
        }

        private void UpdateData(int index)
        {
            resetFields();
            Customer personData = null;
            
            if (allData.TryGetValue(index, out personData))
            {
                Hashtable errors = validateFields(personData);
                if (errors.Count > 0)
                {
                    String allErrors = "";
                    foreach (Object key in errors.Keys)
                    {
                        allErrors += errors[key];
                        ((System.Windows.Forms.TextBox)key).BackColor = System.Drawing.Color.Red;
                    }
                    richTextBoxValidation.Text = allErrors;
                }
                this.textBoxID.Text = index.ToString();
                this.textBoxFirstName.Text = personData.FirstName;
                this.textBoxLastName.Text = personData.LastName;
                this.textBoxPhoneNumber.Text = personData.PhoneNumber;
                this.textBoxStreetNumber.Text = personData.StreetNumber.ToString();
                this.textBoxAddress.Text = personData.Address;
                this.textBoxCity.Text = personData.City;
                this.textBoxProvince.Text = personData.Province;
                this.textBoxCountry.Text = personData.Country;
                this.textBoxEmailAddress.Text = personData.EmailAddress;
                this.textBoxPostalCode.Text = personData.PostalCode;

            }
        }

        // Validate the fields of a Customer and returns a HashTable with the fields and the validation results (if failed any).
        // The HashTable contents is used to update the UI.
        private Hashtable validateFields(Customer cust)
        {
            Hashtable hashTable = new Hashtable();

            // First Name (TextBox) - max lenght 50
            if(cust.FirstName.Length > 50) {
                hashTable.Add(textBoxFirstName, "First Name length greater than 50. Actual length is " + cust.FirstName.Length + ".\n");
            }

            // Last Name (TextBox) - max lenght 50
            if (cust.LastName.Length > 50)
            {
                hashTable.Add(textBoxLastName, "Last Name length greater than 50. Actual length is " + cust.LastName.Length + ".\n");
            }

            // Address (TextBox) - max lenght 50
            if (cust.Address.Length > 50)
            {
                hashTable.Add(textBoxAddress, "Address length greater than 50. Actual length is " + cust.Address.Length + ".\n");
            }

            // City (TextBox) - max lenght 50
            if (cust.City.Length > 50)
            {
                hashTable.Add(textBoxCity, "City length greater than 50. Actual length is " + cust.City.Length + ".\n");
            }

            // Province (TextBox) - max lenght 50
            if (cust.Province.Length > 50)
            {
                hashTable.Add(textBoxProvince, "Province length greater than 50. Actual length is " + cust.Province.Length + ".\n");
            }

            // Country (TextBox) - Text no validation - Canada
            if (cust.Country.Length > 50)
            {
                hashTable.Add(textBoxCountry, "Country length greater than 50. Actual length is " + cust.Country.Length + ".\n");
            }

            // Postal Code  (TextBox) - Max 7 characters ( 6 plus space)
            if (cust.PostalCode.Length > 7)
            {
                hashTable.Add(textBoxPostalCode, "Postal Code length greater than 7. Actual length is " + cust.PostalCode.Length + ".\n");
            }

            return hashTable;
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            String file = @textBoxFileName.Text;

            if (file.Equals(""))
            {
                textBoxFileName.BackColor = System.Drawing.Color.Red;
                MessageBox.Show("Please select a file to import.");
            }
            else
            {
                using (TextFieldParser parser = new TextFieldParser(file))
                {
                    parser.Delimiters = new string[] { "," };
                    while (true)
                    {
                        string[] parts = parser.ReadFields();
                        if (parts == null)
                        {
                            break;
                        }
                        using (CustomerDataModel db = new CustomerDataModel())
                        {
                            DbSet<Customer> customers = db.Customers;
                            Customer cust = new Customer();
                            cust.FirstName = parts[0];
                            cust.LastName = parts[1];
                            int stnum;
                            Int32.TryParse(parts[2], out stnum);
                            cust.StreetNumber = stnum;
                            cust.Address = parts[3];
                            cust.City = parts[4];
                            cust.Province = parts[5];
                            cust.Country = parts[6];
                            cust.PostalCode = parts[7];
                            cust.PhoneNumber = parts[8];
                            cust.EmailAddress = parts[9];

                            // this saves it in the DB to be saved when Save is called
                            customers.Add(cust);
                            db.SaveChanges();
                        }
                    }
                }
                // updates the form and clears the import file name
                ContactForm_Load(sender, e);
                UpdateData(currentIndex);
                textBoxFileName.Text = "";
            }
        }

        private void buttonBrowseFile_Click(object sender, EventArgs e)
        {
            resetFields();

            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                textBoxFileName.Text = openFileDialog1.FileName;
            }
        }
    }
}


# README

## Introduction
My name is Daniel Arantes (call me Dan). I'm brazilian and got my undergraduate degree in Computer Science in 2003. I've been working in IT since 2003 and for the past 12 years working for IBM supporting large clients in the USA. During these years I've played roles as developer, peoplesoft administrator, technical analyst, application architect and database administrator.

## Topics I want to see covered

1. test tools and approaches
2. agile development (at large scales)
3. caching approaches used in industry
4. Docker and virtualization approaches

## Current Development Stack
Currently I've been exploring how Docker can help (and it does!) streamline and improve development.
The technologies below are the ones I'm most comfortable using nowadays.

1. R
2. Oracle
3. Java
